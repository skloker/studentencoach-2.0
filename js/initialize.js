global_obj.categories = new Array();

$(document).ready(function(){
	getCategories();
});

function getCategories(){
	$.getJSON(path, {
		'user_id' : userID,
		'request_id' : 'get_categories',
		'user_token' : '12345',
		'request_parameters' : '12345'
	}, function(result) {
		global_obj.categories = new Array();
		$('#challenge_list_new_categories').empty();
		
		$('#challenge_list_new_categories').append('<option value="" disabled selected>Wähle eine Kategorie...</option>');
		
		$.each(result, function(index, item) {
			if (item['cc_id'] != null){
				global_obj.categories[item['cc_id']] = item;
				$('#challenge_list_new_categories').append(' <option value="' + item['cc_id'] + '">' + item['cc_name'] + '</option>');
			}			
		});
	});
}