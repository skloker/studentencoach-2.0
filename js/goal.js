global_obj.goals = new Array();

function Goal(data) {
	this.id = data.g_id;
	this.title = data.g_name;

	switch (data.g_image) {
	case '1':

		this.url = 'img/' + userID + '/' + this.id + '.jpeg?lastmod='
				+ global_obj['timestamp_refresh'];

		break;
	case '0':
		this.url = '';
		break
	default:
		this.url = data.g_image;
	}

	global_obj.goals[this.id] = this;
}

Goal.prototype.setTitle = function(name) {
	this.title = name;

	$.getJSON(path, {
		'user_id' : userID,
		'request_id' : 'upsert_goal',
		'user_token' : '12345',
		'request_parameters' : {
			g_id : this.id,
			g_name : name,
			g_image : ""
		}
	}, function(result) {
		// Handler fehlt noch
	});
}

Goal.prototype.setUrl = function(newurl) {
	this.url = newurl;
}

Goal.prototype.getUrl = function() {
	var url = 'none';
	if (this.url != undefined) {
		url = "url(" + this.url + ")";
	}
	return url;
}

Goal.prototype.displayPreview = function(target) {
	var html = '<li class="gloal_preview" data-icon="arrow-d" style="background-image:'
			+ this.getUrl()
			+ ';background-position:-50%;background-size:100%"><a style="min-height:100px" onclick="global_obj.goals[\''
			+ this.id + '\'].displayFull(this)">' + this.title
			+ '</a></li>';

	$(target).append(html);
	
	 $( ".gloal_preview" ).droppable({
		 drop: function( event, ui ) {
			 alert('dropped');
		 }
		 });
}

Goal.prototype.displayFull = function(link) {
	if (this.id == 'new') {
		return;
	}

	if (global_obj.goals.current != undefined) {
		global_obj.goals.current.closeFull();
	}

	var listitem = $(link).parent();
	var content_area = $(link);

	content_area.empty();
	content_area.append("<h2>" + this.title + "<h2>");
	content_area
			.append('<div class="overview_single_goal_div" style="background-color:#cccccc;padding:5px"></div>');

	content_area.attr('onclick', 'global_obj.goals[\'' + this.id
			+ '\'].closeFull(this)');

	var single_goal_div = content_area.find('.overview_single_goal_div');

	$(single_goal_div).append(
			'<ul id="overview_single_goal_ul" data-role="listview"></ul>');

	global_obj.goals.current = this;
	getGoalChallenges('#overview_single_goal_ul');

	var single_goal_ul = content_area.find('.overview_single_goal_ul');
	$(single_goal_div).trigger("create");
	$(single_goal_ul).listview();
	$('.ui-li > .ui-btn-inner').css('opacity', '0.8');

	content_area
			.append('<div style="padding-top:30px">'
					+ '<a onclick="global_obj.goals[\'extended\'] = global_obj.goals[\'' + this.id + '\']" href="#challenge_list_p" data-rel="dialog" data-transition="pop" ><img class="goal_setting_icon" src="img/plus.png" /></a>'
					+ '<a onclick=""><img class="goal_setting_icon" src="img/star.png" /></a>'
					+ '<a onclick="goalSettings(event, \'#popup_change_image\')"><img class="goal_setting_icon" src="img/settings.png" /></a>'
					+ '<a onclick="goalSettings(event, \'#popup_change_name\')"><img class="goal_setting_icon" src="img/pen.png" /></a></div>');

	$('.goal_setting_icon').css({
		"width" : "32px",
		"height" : "32px",
		"margin-right" : "20px",
		"background-color" : "#dddddd",
		"border-radius" : "12px",
		"padding" : "5px"
	});

}

Goal.prototype.closeFull = function(link) {
	var content_area = null;

	if (link != undefined) {
		content_area = $(link);
	} else {
		content_area = $($('.overview_single_goal_div').parent());
	}

	content_area.empty();
	content_area.append(this.title);

	content_area.attr('onclick', 'global_obj.goals[\'' + this.id
			+ '\'].displayFull(this)');


	global_obj.goals.current = null;

}

Goal.prototype.deleteGoal = function() {
	$.getJSON(global_obj.path, {
		'user_id' : global_obj.userID,
		'request_id' : 'delete_goal',
		'user_token' : '12345',
		'request_parameters' : {
			g_id : this.id
		}
	}, function(result) {
		loadGoals();
		$('#popup_change_image').popup('close');
	});
}

function loadGoals() {

	$.getJSON(global_obj.path,{
		'user_id' : userID,
		'request_id' : 'get_goals',
		'user_token' : '12345',
		'request_parameters' : '12345'
	}, function(result) {
		$('#overview_goal_ul').empty();

		global_obj.goals = new Array();

		$.each(result, function(index, item) {
			if (item['g_id'] != null) {
				if (global_obj.goals[item['g_id']] == undefined) {
					new Goal(item);
				}
			}
		});

		global_obj.goals['default'] = new Goal({
			g_id : 'default',
			g_name : 'ohne Ziel',
			g_image : 'img/default.jpg'
		});

		global_obj.goals['new'] = new Goal({
			g_id : 'new',
			g_name : 'Neues Ziel anlegen<br /><input type="text" id="overview_goal_new_goal_title" placeholder="Was willst du erreichen?"/><br \><a data-role="button" data-icon="plus" onclick="addGoal()">Anlegen</button>',
			g_image : null
		});

		loadChallenges();
		displayGoals();
	});
}

function displayGoals() {
	$('#overview_goal_ul').empty();

	for (key in global_obj.goals) {
		if (key != 'current') {
			var cur_obj = global_obj.goals[key];
			cur_obj.displayPreview("#overview_goal_ul");
		}
	}

	$('#overview_goal_ul').listview("refresh");
}

function goalSettings(e, selector) {
	e.stopPropagation();

	global_obj['popup_selector'] = selector;

	$(selector).popup("open", {
		transition : "pop",
		positionTo : "window"
	});

	var options = {
		success : function(evt) {
			// alert('success');
		},
		complete : function(xhr) {
			if (xhr.responseText != '{"status":"success"}'){
				alert(xhr.responseText);
			}

			$.getJSON(global_obj.path, {
				'user_id' : global_obj.userID,
				'request_id' : 'upsert_goal',
				'user_token' : '12345',
				'request_parameters' : {
					g_id : global_obj.goals.current.id,
					g_name : global_obj.goals.current.title,
					g_image : "1"
				}
			}, function(result) {
				$(global_obj['popup_selector']).popup("close");
				global_obj['popup_selector'] = null;

				var timestamp = new Date();
				timestamp = timestamp.getTime();

				global_obj['timestamp_refresh'] = timestamp;

				loadGoals();
			});
		}
	};

	$('#change_image_form').unbind("submit");
	$('#change_image_form').submit(function() {
		$(this).ajaxSubmit(options);
		return false;
	});

	$('#change_image_goal_id').val(global_obj.goals.current.id);
	$('#change_image_user_mail').val(userID);
}

function doGoalChange(what, e) {
	e.stopPropagation();

	switch (what) {
	case 'title':
		global_obj.goals.current.setTitle($('#new_name').val());
		$('#popup_change_name').popup("close");
		break;
	case 'image':
		global_obj.goals.current.setUrl($('#new_image').val());
		$('#popup_change_image').popup("close");
		break;
	case 'delete':
		global_obj.goals.current.deleteGoal();
	default:
		// nothing now
	}

	displayGoals();
}

function addGoal() {
	var new_goal = $('#overview_goal_new_goal_title').val();
	$('#overview_goal_new_goal_title').empty();

	if (new_goal != '') {
		$.getJSON(global_obj.path, {
			'user_id' : global_obj.userID,
			'request_id' : 'upsert_goal',
			'user_token' : '12345',
			'request_parameters' : {
				g_id : 'undefined',
				g_name : new_goal,
				g_image : "0"
			}
		}, function(result) {
			loadGoals();
		});
	}

}