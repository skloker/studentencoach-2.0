global_obj.challenges = new Array();
global_obj.challenges.started = new Array();

function Challenge(data) {
	this.id = data.c_id;
	this.name = data.c_name;
	this.usage = data.c_usage;
	this.description = data.c_description;
	this.category = data.c_cc_id;
	this.is_private = data.c_private;

	global_obj.challenges[this.id] = this;
}

Challenge.prototype.getName = function() {
	return this.name;
}

Challenge.prototype.getCategory = function() {
	var c_name = global_obj.categories[this.category].cc_name;
	return c_name;
}

Challenge.prototype.displayAsSearchResult = function() {
	var html = '<li><a onclick="global_obj.challenges[\'' + this.id
			+ '\'].displayFull()">' + this.getName() + '</a></li>';
	$('#challenge_list_search_ul').append(html);
}

Challenge.prototype.displayFull = function() {
	global_obj.challenges['current'] = this;

	$('#challenge_view_name').html(this.name);
	$('#challenge_view_category').html(this.getCategory());
	$('#challenge_view_usage').html(this.usage);
	$('#challenge_view_description').html(this.description);

	$.mobile.changePage('#challenge_view_p');
}

Challenge.prototype.start = function() {
	var start_date_string = $('#challenge_view_start_date').val();
	var duration = $('#challenge_view_duration').val();

	var start_date = new Date();
	start_date.setFullYear(start_date_string.substring(6, 10),
			start_date_string.substring(3, 5) - 1, 
			start_date_string.substring(0, 2));

	if (start_date < new Date()) {
		alert('Date is to low!');
	} else if (duration < 1) {
		alert('Duration ist to low');
	} else {
		var end_date = new Date(start_date);
		end_date.setDate(end_date.getDate() + parseInt(duration));
		
		$.getJSON(path, {
			'user_id' : userID,
			'request_id' : 'start_challenge',
			'user_token' : global_obj['token'],
			'request_parameters' : {
				c_id : this.id,
				start : start_date,
				end : end_date
			}
		}, function(result) {
			if (global_obj.goals.extended.id != 'default'){
				$.getJSON(path, {
					'user_id' : userID,
					'request_id' : 'add_challenge_to_goal',
					'user_token' : global_obj['token'],
					'request_parameters' : {
						cs_id : result.new_id,
						g_id : global_obj.goals.extended.id
					}
				}, function(result){
					// hope for success
				});
			}
			
			$.getJSON(path, {
				'user_id' : userID,
				'request_id' : 'join_challenge',
				'user_token' : global_obj['token'],
				'request_parameters' : {
					cs_id : result.new_id
				}
			}, function(result){
				alert('Challenge wurde gestartet!');
				loadGoals();
				$.mobile.changePage('#overview_p');
			});
			
		});
	}
}

function ChallengeStarted(data) {
	this.started_id = data.cs_id;
	this.startdate = parseDate(data.cs_startdate);
	this.enddate = parseDate(data.cs_enddate);
	this.goal = data.cgm_g_id;
	this.challenge_id = data.c_id;
	this.challenge_obj = global_obj.challenges[data.c_id];

	global_obj.challenges.started[this.started_id] = this;
}

ChallengeStarted.prototype.displayPreview = function(list) {
	var name = global_obj.challenges[this.challenge_id].getName();
	var html = '<li class="challengePreview"><a data-iconpos="left" onclick="global_obj.challenges.started[\''
			+ this.started_id + '\'].displayFull(event)">' + name + '</a></li>';

	$(list).append(html);
	 $( ".challengePreview" ).draggable({ axis: "y" , containment: "#overview_goal_ul"});
}

ChallengeStarted.prototype.displayFull = function(e) {
	if (e != null){
		e.stopPropagation();
	}
	
	global_obj.challenges.started['current'] = this;
	global_obj.challenges['current'] = this;

	$('#challenge_started_view_name').html(this.challenge_obj.name);
	$('#challenge_started_view_category').html(this.challenge_obj.getCategory());
	$('#challenge_started_view_usage').html(this.challenge_obj.usage);
	$('#challenge_started_view_description').html(this.challenge_obj.description);
	
	$('#challenge_started_starts').html(this.startdate);
	$('#challenge_started_ends').html(this.enddate);
	
	global_obj.today = new Date();
	global_obj.today.setHours(0);
	global_obj.today.setMinutes(0);
	global_obj.today.setSeconds(0);
	global_obj.today.setMilliseconds(0);
	
	if (global_obj.today < this.startdate){
		$('#challenge_started_flag_btn_dis').show();
		$('#challenge_started_flag_btn').hide();
	} else if (global_obj.today > this.enddate){
		$('#challenge_started_flag_btn_dis').show();
		$('#challenge_started_flag_btn').hide();
	} else {
		$.getJSON(global_obj['path'], {
			'user_id' : global_obj['userID'],
			'request_id' : 'get_done_flags',
			'user_token' : global_obj['token'],
			'request_parameters' : {
				cs_id : this.started_id
			}
		}, function(result){
			var counter = 0;
			
			var flag_allowed = true;
			
			if (result[0] != '-1'){
				this.done_flags = result;
				
				$.each(this.done_flags, function(count, item){
					counter = counter + 1;
					
					if ((parseDate(item.cdf_date)).getTime() == global_obj.today.getTime()){
						flag_allowed = false;
					}
				});
			} else {
				flag_allowed = true;
			}
			
			

				if (flag_allowed == false){
					$('#challenge_started_flag_btn_dis').show();
					$('#challenge_started_flag_btn').hide();
				} else {
					$('#challenge_started_flag_btn').show();
					$('#challenge_started_flag_btn_dis').hide();
				}		
	
			
			$('#challenge_started_counter').html(counter);
			$.mobile.changePage('#challenge_started_view_p');
		});	
	}
}

ChallengeStarted.prototype.setTodayFlag = function(){
	$.getJSON(global_obj['path'], {
		'user_id' : global_obj['userID'],
		'request_id' : 'set_done_flag',
		'user_token' : global_obj['token'],
		'request_parameters' : {
			cs_id : this.started_id,
			date : global_obj.today
		}
	}, function(result){
		global_obj.challenges.started.current.displayFull();
	});
}

function getGoalChallenges(list) {
	for (key in global_obj.challenges.started) {
		var item = global_obj.challenges.started[key];
		
		if (key != 'current'){
			if (item['goal'] == global_obj.goals.current.id) {
				item.displayPreview(list);
			} else if ((global_obj.goals.current.id == 'default')
					&& (item.goal == '0')) {
				item.displayPreview(list);
			}
		}
	}
}

function createNewChallenge() {
	global_obj.cache = {
			name : $('#challenge_list_new_name').val(),
			usage : $('#challenge_list_new_usage').val(),
			description : $('#challenge_list_new_description').val(),
			category_id : $('#challenge_list_new_categories').val(),
			'private' : '0' 
	}
	

	$('#challenge_list_new_name').val('');
	$('#challenge_list_new_usage').val('');
	$('#challenge_list_new_description').val('');

	$.getJSON(global_obj.path, {
		'user_id' : global_obj.userID,
		'request_id' : 'insert_challenge',
		'user_token' : global_obj.token,
		'request_parameters' : {
			'name' : global_obj.cache.name,
			'usage' : global_obj.cache.usage,
			'description' : global_obj.cache.description,
			'category' : global_obj.cache.category_id
		}
	}, function(result) {
		$.getJSON(global_obj.path, {
			'user_id' : global_obj.userID,
			'request_id' : 'insert_challenge_creator',
			'user_token' : global_obj.token,
			'request_parameters' : {
				'c_id' : result.new_id,
				'private' : global_obj.cache['private']
			}
		}, function(inner_result){
			var data = {
					'c_id' : result.new_id,
					'c_name' : global_obj.cache.name,
					'c_usage' : global_obj.cache.usage,
					'c_description' : global_obj.cache.description,
					'c_cc_id' : global_obj.cache.category_id,
					'c_private' : global_obj.cache['private']
			}
			
			global_obj.challenges[result.new_id] = new Challenge(data);
			global_obj.challenges[result.new_id].displayFull();
			
			global_obj.cache = null;
		});		
	});
}

function challengeSearch(input) {

	if (input.value != '') {

		$.getJSON(path, {
			'user_id' : userID,
			'request_id' : 'search_challenges',
			'user_token' : '12345',
			'request_parameters' : {
				search_string : input.value
			}
		}, function(result) {
			var listItem = null;
			$('#challenge_list_search_ul').empty();

			$.each(result, function(index, item) {
				if (item['c_id'] != undefined) {
					var proposal = new Challenge(item);
					proposal.displayAsSearchResult();
				}
			});

			$('#challenge_list_search_ul').listview('refresh');

		});
	} else {
		$('#challenge_list_search_ul').empty();
	}
}

function loadChallenges() {
	$.getJSON(path, {
		'user_id' : userID,
		'request_id' : 'get_challenges',
		'user_token' : '12345',
		'request_parameters' : '12345'
	}, function(result) {

		global_obj.challenges = new Array();
		global_obj.challenges.started = new Array();

		$.each(result, function(index, item) {
			if (item['cs_id'] != undefined) {
				new Challenge(item);
				new ChallengeStarted(item);
			}
		});

	});
}