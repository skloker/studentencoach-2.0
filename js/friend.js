function Friend(data){
	
	this.active = data.f_active;
	this.reactive = data.f_reactive;
	this.date = data.f_date;
	this.nickname = data.u_nickname;
	this.mail = data.u_mail;
	
	global_obj.friends[this.mail] = this;
}

Friend.prototype.showInProfile = function(){
	
}

Friend.prototype.invite = function(){
	
}

function getFriendsList(){
	
}

function getFriendsRequests(){
	
}

function acceptInvitation(){
	
}

function searchUser(string){
	if (string.length > 0){
		$.getJSON(global_obj.path, {
			'user_id' : global_obj.userID,
			'request_id' : 'search_user',
			'user_token' : global_obj.token,
			'request_parameters' : {
				'search_string' : string
			}
		}, function(result){
			$('#friend_list_search_results').empty();
			
			if (result[0] != '-1'){
				for (key in result){
					var item = result[key];
					
					var html = '<li><a href="" onclick="showUserProfile(\'' + item.u_mail + '\')">' + item.u_nickname + '</a></li>';
					$('#friend_list_search_results').append(html);
				}
			} else {
				
			}
			
			try {
				$('#friend_list_search_results').listview('refresh');
			} catch(e) {
				//do nothing
			}
		});
	} else {
		$('#friend_list_search_results').empty();
	}
		
}

function showUserProfile(mail){
	$.getJSON(global_obj.path, {
		'user_id' : global_obj.userID,
		'request_id' : 'get_user',
		'user_token' : global_obj.token,
		'request_parameters' : {
			'mail' : mail
		}
	}, function(result){
		$('#user_profile_name').html(result[0].u_nickname);
		$('#user_profile_mail').html(result[0].u_mail);
		
		if ((result[0].af_active == null) && (result[0].rf_active == null)){
			//No friendship, no request
			$('#user_profile_friendship').html(result[0].u_nickname + ' is not your friend by now. You can send him a friends request to start challenges with him.');
			$('#user_profile_friend_request').html('<button onclick="sendFriendRequest(\'' + result[0].u_mail + '\')">Send friends request!</button>');
		} else {
			if ((result[0].af_date == null) && (result[0].rf_date == null)){
				if ((result[0].af_active == global_obj.userID) || (result[0].rf_active == global_obj.userID)){
					//Friend Request by me --> wait
					$('#user_profile_friendship').html('You have send an friends request, which is still not accepted by ' + result[0].u_nickname);
					$('#user_profile_friend_request').empty();
				} else {
					//Friend Request by selected user --> offer accept
					$('#user_profile_friendship').html(result[0].u_nickname + ' has send you an friends request. Accept if you want to challenge together!');
					$('#user_profile_friend_request').html('<button onclick="acceptFriendRequest(\'' + result[0].u_mail + '\')">Accept friends request!</button>');
				}
			} else {
				//Friendship
				$('#user_profile_friendship').html('Friends since ' + result[0].f_date);
				$('#user_profile_friend_request').empty();
			}
		} 
		
		$.mobile.changePage('#user_profile_p');
	});
}

function sendFriendRequest(mail){
	$.getJSON(global_obj.path, {
		'user_id' : global_obj.userID,
		'request_id' : 'send_friend_request',
		'user_token' : global_obj.token,
		'request_parameters' : {
			'mail' : mail
		}
	}, function(result){
		
	});
}

function acceptFriendRequest(){
	
}