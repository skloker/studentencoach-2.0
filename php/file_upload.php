<?php

$userID = $_POST['user_mail'];
$goalID = $_POST['g_id'];

//Erlaubte Endungen
$allowedExts = array("gif", "jpeg", "jpg", "png");

//Endung holen
$extension = strtolower(end(explode(".", $_FILES["file"]["name"])));


if ((($_FILES["file"]["type"] == "image/gif") 
	|| ($_FILES["file"]["type"] == "image/jpeg") 
	|| ($_FILES["file"]["type"] == "image/jpg") 
	|| ($_FILES["file"]["type"] == "image/pjpeg") 
	|| ($_FILES["file"]["type"] == "image/x-png") 
	|| ($_FILES["file"]["type"] == "image/png")) 
	&& ($_FILES["file"]["size"] < 500000) 
	&& in_array($extension, $allowedExts)) {

	if ($_FILES["file"]["error"] > 0) {
		echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {

		//(if (file_exists("upload/" . $_FILES["file"]["name"])) {
		if (file_exists("img/" . $userID . "/" . $goalID . ".jpg")) {
			//echo $_FILES["file"]["name"] . " already exists. ";

			//löscht File
			unlink("img/" . $userID . "/" . $goalID . ".jpeg");
		} else {
			move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $_FILES["file"]["name"]);
			//echo "Stored in: " . "upload/" . $_FILES["file"]["name" ];

			$size = getimagesize("upload/" . $_FILES["file"]["name"]);

			if ($size[0]>64 || $size[1]>48) {

				switch($extension) {
					case "png" :
						$img = imagecreatefrompng("upload/" . $_FILES["file"]["name"]);
						break;
					case "gif" :
						$img = imagecreatefromgif("upload/" . $_FILES["file"]["name"]);
						break;
					case "jpg" :
						$img = imagecreatefromjpeg("upload/" . $_FILES["file"]["name"]);
						break;
					case "jpeg" :
						$img = imagecreatefromjpeg("upload/" . $_FILES["file"]["name"]);
						break;
				}

				$dst_img = imagecreatetruecolor(640,480);
				
				if (!file_exists("img/" . $userID)) {
   					mkdir("img/" . $userID);
				}

				imagecopyresampled($dst_img, $img, 0, 0, 0, 0, 640, 480, $size[0], $size[1]);
				imagejpeg($dst_img, "img/" . $userID . "/" . $goalID . ".jpeg");

				imagedestroy($img);
				imagedestroy($dst_img);

				unlink("upload/" . $_FILES["file"]["name"]);
				
				echo json_encode(array("status"=>"success"));
			}
		}
	}
} else {
	echo "Invalid file<br>";
	
	echo "Upload: " . $_FILES["file"]["name"] . "<br>";
		echo "Type: " . $_FILES["file"]["type"] . "<br>";
		echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
		echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
}
?>