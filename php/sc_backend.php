<?php
//Report all errors
error_reporting(-1);
//if (!ini_get('display_errors')) {
//    ini_set('display_errors', '1');
//}
//Do not allow any caching
header("Content-type: application/x-www-form-urlencoded");
header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once 'db_access.class.php';

$database_information = mysql_connect('mysql2.netbeat.de', 'sql15883', 'admin') OR die(mysql_error());

$request_id = myRealEscape($_REQUEST["request_id"]);
$user_id = myRealEscape($_REQUEST["user_id"]);
$user_token = myRealEscape($_REQUEST["user_token"]);
$request_parameters = $_REQUEST["request_parameters"];

$objekt = new db_access();

switch ($request_id){
	case 'get_goals' :
		$sql = "select g_id, g_name, g_image
				from sc_goal
				where g_u_mail = '".$user_id."'";
	
		break;
	case 'upsert_goal' :
		if ($request_parameters['g_id'] != 'undefined'){
			$sql = "update sc_goal
					set g_name = '".$request_parameters['g_name']."', g_image = '".$request_parameters['g_image']."'
					where g_id = '".$request_parameters['g_id']."'";
		} else {
			$sql = "insert into sc_goal (g_name, g_image, g_u_mail)
					values ('".$request_parameters['g_name']."', '".$request_parameters['g_image']."', '".$user_id."')";
		}
		
		break;
	case 'delete_goal' :
		$sql = "delete from sc_goal
				where g_id = '".$request_parameters['g_id']."';
				delete from sc_challenge_goal_membership
				where cgm_g_id = '".$request_parameters['g_id']."'";
				
		break;
	case 'insert_challenge' :
		$sql = "insert into sc_challenge (c_name, c_usage, c_description, c_cc_id)
				values ('".$request_parameters['name']."', '".$request_parameters['usage']."', 
				'".$request_parameters['description']."', '".$request_parameters['category']."')"; 
		
		break;
	case 'insert_challenge_creator' :
		$sql = "insert into sc_challenge_creator (cr_creator, cr_c_id, cr_private)
				values ('".$user_id."', '".$request_parameters['c_id']."', '".$request_parameters['private']."')"; 
		
		break;
	case 'get_challenges' :
		$sql = "select cs_id, cs_startdate, cs_enddate, c_id, c_name, cgm_g_id, c_cc_id, c_usage, c_description, cr_creator, cr_private 
				from sc_challenge_participant
				left join sc_challenge_started on cp_cs_id = cs_id
				left join sc_challenge_goal_membership on cs_id = cgm_cs_id
				left join sc_challenge on cs_c_id = c_id
				left join sc_challenge_creator on c_id = cr_c_id
				where cp_u_mail = '".$user_id."'";
				
		break;
	case 'search_challenges' :
		$sql = "select * from sc_challenge
				left join sc_challenge_creator on c_id = cr_c_id
				where (cr_private <> '1' or cr_creator = '".$user_id."')
				and c_name like '%".$request_parameters['search_string']."%'
				LIMIT 0,20";
				
		break;
	case 'start_challenge' :
		$sql = "insert into sc_challenge_started (cs_c_id, cs_startdate, cs_enddate)
				values('".$request_parameters['c_id']."','".formatDate($request_parameters['start'])."','".formatDate($request_parameters['end'])."')";
				
		break;
	case 'join_challenge' :
		$sql = "insert into sc_challenge_participant (cp_u_mail, cp_cs_id)
				values('".$user_id."','".$request_parameters['cs_id']."')";
				
		break;
	case 'add_challenge_to_goal' :
		$sql = "insert into sc_challenge_goal_membership (cgm_g_id, cgm_cs_id)
				values('".$request_parameters['g_id']."','".$request_parameters['cs_id']."')";
				
		break;
	case 'get_done_flags' :
		$sql = "select * from sc_challenge_done_flag
				where cdf_cs_id = '".$request_parameters['cs_id']."'
				and cdf_u_mail = '".$user_id."'";
				
		break;
	case 'set_done_flag' :
		$sql = "insert into sc_challenge_done_flag (cdf_cs_id, cdf_u_mail, cdf_date)
				values('".$request_parameters['cs_id']."','".$user_id."','".formatDate($request_parameters['date'])."')";
				
		break;
	case 'get_categories' :
		$sql = "select * from sc_challenge_category";
		
		break;
	case 'search_user' :
		$sql = "select * from sc_user
				where u_nickname like '%".$request_parameters['search_string']."%'
				or u_mail like '%".$request_parameters['search_string']."%'
				LIMIT 0,20";
				
		break;
	case 'get_user' :
		$sql = "select u_mail, u_nickname, active.f_date as af_date, reactive.f_date as rf_date, active.f_active as af_active, reactive.f_active as rf_active from sc_user
				left join sc_friends as active on u_mail = active.f_active
				left join sc_friends as reactive on u_mail = reactive.f_reactive
				where u_mail = '".$request_parameters['mail']."'
				or ((active.f_active = '".$request_parameters['mail']."' and active.f_reactive = '".$user_id."')
				or (reactive.f_reactive = '".$request_parameters['mail']."' and reactive.f_active = '".$user_id."'));";
			
		break;
	case 'send_friend_request' :
		$sql = "insert into sc_friends (f_active, f_reactive)
				values('".$user_id."','".$request_parameters['mail']."')";
			
		break
	case 'accept_friend_request' :
		$sql = "update sc_friends
				set f_date = '".$request_parameters['today']."'
				where f_active = '".$request_parameters['mail']."'
				and f_reactive = '".$user_id."'";
			
		break;
	default:
		//request_id gibts nicht	
}

$objekt -> query($sql);

$array = $objekt -> fetch_array();

if (empty($array) && $objekt->insert_id != null){
	$array["new_id"] = $objekt->insert_id;
}

$objekt -> free();
$objekt -> disconnect();


if (empty($array)) {
	echo $_GET['jsoncallback'];
	echo '('.json_encode(array("-1")).');';	
} else {
	echo $_GET['jsoncallback'];
	echo '('.json_encode($array).');';
}

function myRealEscape($string) {
	if (get_magic_quotes_gpc()) {
		return mysql_real_escape_string(stripslashes($string));
	} else {
		return mysql_real_escape_string($string);
	}
}

function formatDate($input_date) {
	return date( 'Y-m-d', strtotime($input_date));
}
?>