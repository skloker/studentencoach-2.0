<?php


/**
 *
 * <code>
 * <?php
 * require_once 'class/db_access.class.php';
 *
 * $objekt = new db_access();
 * $objekt->connect();
 * $objekt->query($query);
 * $array=$objekt->fetch_array();
 * $objekt->free();
 * $objekt->disconnect();
 * ?>
 * </code>
 */
class db_access {

    var $conn;

   
    var $res;

   
    var $anz;

    
    var $insert_id;

    public function getAnz() {
        return $this->anz;
    }

   
    public function getConn() {
        return $this->conn;
    }

    
    function __construct() {
        //Aufbauen der Verbindung
        $this->conn = new mysqli('mysql2.netbeat.de', 'sql15883', 'admin', 'sql15883');
        //Check auf Fehler
        if (mysqli_connect_errno() != 0) {
            die('Could not connect to MySQL: ' . mysqli_connect_error());
        }
    }

    public function connect() {
        return true;
    }

    
    function disconnect() {
        $this->conn->close();
    }

   
    function query($query) {
        //Abfrage ausfuehren
        $this->res = $this->conn->query($query);
		
		if (!$this->res){
			$this->res = $this->conn->multi_query($query);
		}
		
        //'anz' setzen
         if (is_object($this->res)){
			$this->anz = $this->res->num_rows;
         }   
		 
		 if ($this->conn->insert_id != 0){
		 	$this->insert_id = $this->conn->insert_id;
		 }

        //Fehler ausgeben
        if (!empty($this->res->error))
            echo $this->res->error;
        //Resultset zurueckgeben
        return $this->res;
    }

    function fetch_array() {
        //Wenn 'res' ein Objekt ist
        if (is_object($this->res)) {
            //Hole alle Zeilen
            $array = array();
            
            while ($zeile = $this->res->fetch_assoc()) {
                //Und schreibe Sie in ein array
                $array[] = $zeile;
            }
            //Zurueckgeben des Arrays
            return $array;
        }else
        //Sonst 'falsch' zurueckgeben
            return false;
    }

    function free() {
        //Pruefung 'res' auf Objekt
        if (is_object($this->res)) {
            //Resueltset schlieÃŸen
            $this->res->close();
        }
        //wahr zurueckgeben
        return true;
    }

    
    function delete($query) {
        $this->conn->query($query);
        if ($this->conn->affected_rows >= 1) {
            return $this->conn->affected_rows;
        }else
            return false;
    }

   
    function insert($query) {
        $this->conn->query($query);
        $this->insert_id = $this->conn->insert_id;
        if (!empty($this->insert_id)) {
            return $this->insert_id;
        } else {
            return false;
        }
    }

    function update($query) {
        $this->conn->query($query);
        if ($this->conn->affected_rows >= 1) {
            return $this->conn->affected_rows;
        }else
            return false;
    }

}

?>
